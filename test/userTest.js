process.env.DB_TEST
const server = require('../index');
const User = require('./../models/user');
const chai = require('chai');
const chaihttp = require('chai-http');
const should = chai.should();

chai.use(chaihttp);
chai.should();

// delete all data after test
// before(done => {
//     User.deleteMany({})
//     .then(result => {
//         done();
//     })
//     .catch(err => {
//         console.log(err)
//     })
// })
// Create user before testing
// delete user after unit testing
afterEach(done => {
    User.deleteMany({})
        .then(result => {
            done();
        })
        .catch(err => {
            console.log(err)
        })
})


// Register Variabel
let register = {name: "angel", email: "leexiao@example.com", password: "password"};


// 1. Test Register Email
describe("/ Post new User", () =>{
    it("It should Register new User", (done) => {
        console.log(register)
        chai.request(server)
        .post("/api/user/register")
        .send(register)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal("successfully register!");
            done();
        })
    })
})


// 1. Get Status(422)
describe("/ GET ERROR STATUS(422)", () => {
    it("it should get error, because email already taken.", (done) => {
        const user = new User({name: "lee", email: "leexiao@example.com", password: "password"})
        user.save((error, user)=>{
            chai.request(server)
            .post("/api/user/register")
            .send(user)
            .end((err, res)=> {
                res.should.have.status(422);
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal("User already Registered.");
                done();
            })
        })
    })
})


// 2. Test Login
describe("/ Login User", ()=> {
    it("It should let user login", (done) => {
        let login = new User({name: "login test", email: "login@test.com", password: "password"});
        login.save();
        chai.request(server)
            .post("/api/user/login")
            .send({email: "login@test.com", password: "password"})
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })
    })
})


// 2. Get LOGIN Status(422)
describe("/ GET LOGIN ERROR STATUS(422)", () => {
    it("it should get error, because email are not Register.", (done) => {
        const user = {name: "angel", email: "test@example.com", password: "password"};
        chai.request(server)
        .post("/api/user/login")
        .send(user)
        .end((err, res)=> {
            res.should.have.status(421);
            res.body.should.have.property('success').equal(false);
            res.body.should.have.property('message').equal('Email are not Registered!');
            done();
        })
    })
})


// 2. Get LOGIN Status(421)
describe("/ GET LOGIN ERROR STATUS(421)", () => {
    it("it should get error, because password wrong.", (done) => {
        const login = new User({name: "login test", email: "login@test.com", password: "password"});
        login.save((error, login)=>{
            chai.request(server)
            .post("/api/user/login")
            .send({email: login.email, password: "password177"})
            .end((err, res)=> {
                res.should.have.status(422);
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal("wrong password");
                done();
            })
        })
    })
})


// 3. SHOW ALL USER
describe('/Get the show path from User', ()=>{
    it("should show all user list", (done)=>{
        chai.request(server)
        .get('/api/user/')
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal("here is your user list: ");
            res.should.be.an("object");
            done();
        })
    })
})
