const server = require('../index');
const chai = require('chai');
const chaihttp = require('chai-http');
const should = chai.should();
// get Todo Model
const Todo = require('../models/todo');
// get User Model
const User = require('../models/user');
// get Faker for create fake data
// const faker = require('faker');
const bcrypt = require('bcryptjs');
// var name = faker


chai.use(chaihttp);
chai.should();


afterEach(done => {
    Todo.deleteMany({})
        .then(result => {
            done();
        })
        .catch(err => {
            console.log(err)
        })
})



// 1. to test the root path
describe('/Get the root path', ()=>{
    it("should get the index page", (done)=>{
        chai.request(server)
        .get('/')
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal("Here is your Todo API!");
            done();
        })
    })
})

// 2. Get the show list
describe('/Get the show path from todo', ()=>{
    it("should show all todo list", (done)=>{
        chai.request(server)
        .get('/api/todo/show')
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal("here is your Todo List:");
            res.should.be.an("object");
            done();
        })
    })
})


// 3. Create a new todo
describe('/POST todo Routes', ()=>{
    it("it should create a todo", (done)=>{
        let user = new User({name: "user 1", email: "test@email.com", password: "password"});
        user.save();
        let login = user.generateAuthToken();
        let token = `bearer ${login}`;
        const newTodo = {task: "task test", status: "false"};
        chai.request(server)
        .post('/api/todo/new')
        .set("token", token)
        .send(newTodo)
        .end((err, res) =>{
            res.should.have.status(200);
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('message').equal("Success create new task !");
            res.should.be.an("object");
            done();
        })
    })
})


// 3. Get Error Status from Create Function
describe('/POST todo Routes', ()=>{
    it("it should create a todo", (done)=>{
            let user = new User({name: "user 1", email: "test@email.com", password: "password"});
            user.save();
            let login = user.generateAuthToken();
            let token = `bearer ${login}`;
            const newTodo = {task: "", status: "false"};
            chai.request(server)
            .post('/api/todo/new')
            .set("token", token)
            .send(newTodo)
            .end((err, res) =>{
            res.should.have.status(400);
            done();
        })
    })
})


// 4. Shouldn't Update the Todo based on Id
describe('/Get Error messages from Update', ()=>{
    it("it shouldn't update a todo based on Id we get from Params", (done)=>{
        let user = new User({name: "user 1", email: "test@email.com", password: "password"});
        user.save();
        let login = user.generateAuthToken();
        let token = `bearer ${login}`;
        const newTodo = new Todo({task: "task new", status: "false", user: user._id});
        newTodo.save((error, newTodo)=>{
            chai.request(server)
            .put('/api/todo/update/5d7775234b88752bee92aaa2')
            .set("token", token)
            .send({task: "task update", status: "false"})
            .end((err, res)=>{
                res.should.have.status(422);
                done();
            })
            if(error) return res.status(422).json({error: error});
        })
    })
})


// 4. Update the Todo based on Id 
describe('/Put Update Todo List', ()=>{
    it("it should update the todo based on Id we get from params", (done)=>{
        let user = new User({name: "user 1", email: "test@email.com", password: "password"});
        user.save();
        let login = user.generateAuthToken();
        let token = `bearer ${login}`;
        const newTodo = new Todo({task: "task new", status: "false", user: user._id});
        newTodo.save((error, newTodo)=>{
            console.log(newTodo._id)
            console.log(newTodo.user)
            chai.request(server)
            .put('/api/todo/update/' + newTodo._id)
            .set("token", token)
            .send({task: "task update", status: "false"})
            .end((err, res)=>{
                res.should.have.status(200);
                done();
            })
            if(error) return res.status(422).json({error: error});
        })
    })
})

// 4. Update blank Todo
describe('/Get Error status(400) from Update', ()=>{
    it("it should get update error status(400)", (done)=> {
        let user = new User({name: "user 1", email: "test@email.com", password: "password"});
        user.save();
        let login = user.generateAuthToken();
        let token = `bearer ${login}`;
        const newTodo = new Todo({task: "task new", status: "false", user: user._id});
        newTodo.save((error, newTodo)=>{
            console.log(newTodo)
            console.log(newTodo.user)
            chai.request(server)
            .put('/api/todo/update/' + newTodo._id)
            .set("token", token)
            .send({task: "", status: "false"})
            .end((err, res)=>{
                res.should.have.status(400);
                done();
            })
            if(error) return res.status(422).json({error: error});
        })
    })
})


// 5. Delete the Todo base on Id
describe('/DELETE todo Routes', ()=>{
    it("it should be able to delete a todo based on Id we get from params", (done)=>{
        let user = new User({name: "user 1", email: "test@email.com", password: "password"});
        user.save();
        let login = user.generateAuthToken();
        let token = `bearer ${login}`;
        const newTodo = new Todo({task: "task new", status: "false", user: user._id});
        newTodo.save((error, newTodo)=>{
            chai.request(server)
            console.log(newTodo._id)
            .delete('/api/todo/delete/' + newTodo._id)
            .set("token", token)
            .end((err, res)=>{
                res.should.have.status(200);
                done();
            })
            if(error) return res.status(422).json({error: error});
        })
    })
})

// 5. Delete Todo Error Status.(422)
// describe('/ Get Error DELETE todo Routes', ()=>{
//     it("it shouldn't be able to delete a todo based on Id we get from params", (done)=>{
//         let user = new User({name: "user 1", email: "test@email.com", password: "password"});
//         user.save();
//         let login = user.generateAuthToken();
//         let token = `bearer ${login}`;
//         const newTodo = new Todo({task: "task new", status: "false", user: user._id});
//         newTodo.save((error, newTodo)=>{
//             console.log(newTodo)
//             console.log(newTodo._id)
//             chai.request(server)
//             .delete('/api/todo/delete/5d7228a8bea27c37e456221b')
//             .set("token", token)
//             .end((err, res)=>{
//                 res.should.have.status(422);
//                 done();
//             })
//             if(error) return res.status(422).json({error: error});
//         })
//     })
// })


// 6. Show Todo Base on Id
// describe('/Get the show path from todo', ()=>{
//     it("should show all todo list", (done)=>{
//         chai.request(server)
//         .get('/api/todo/show/5d635a9a0f354e2b572ddcf0')
//         .end((err, res)=>{
//             res.should.have.status(200);
//             res.body.should.have.property('success').equal(true);
//             res.body.should.have.property('message').equal("Detail Todo List:");
//             res.should.be.an("object");
//             done();
//         })
//     })
// })
