const Todo = require('./../models/todo');
const User = require('./../models/user');
const { success, errorMessage } = require('./../helper/response');
const { validationTodo } = require('./../helper/validator');

// 1. Create new Todo
async function createTodo(req, res){
    // checking if there is any error inputs
    const { error } = validationTodo(req.body);
    if(error) return res.status(400).json(error.details[0].message);

    let user = await User.findById(req.user._id);

    // New and Create FUnction
    const todo = new Todo({task: req.body.task, status: req.body.status, user: req.user._id});
    // save new todo in todo schema
    const todo_result = await todo.save();

    // save new Todo in User Schema
    user.todo.push(todo);
    await user.save();

    res.status(200).json(success(todo_result, "Success create new task !"))
}


// 2. Show all Todo
async function showTodo(req, res){

    const todoList = await Todo.find({});
    res.status(200).json(success(todoList, "here is your Todo List:"))
}


// 3. Show Todo By Id
async function show_by_id(req, res){
    // check the token
    let user = await User.findById(req.user._id);
    if(!user) return res.status(422).json(errorMessage("user not found"))

    // find todo from Id
    const todo = await Todo.findById(req.params.id).populate("user", "name");
    res.status(200).json(success(todo, "Detail Todo List:"));
}




// 4. update Todo
async function updateTodo(req, res){
    // checking if there is any error inputs
        const { error } = validationTodo(req.body);
        if(error) return res.status(400).json(error.details[0].message);

    let user = await User.findById(req.user._id);
    if(!user) return res.status(422).json(errorMessage("user not found"))

    const updateTodo = await Todo.findByIdAndUpdate(req.params.id,{
        $set: {task: req.body.task, status: req.body.status, user: req.user._id}
    }, { new: true }).populate('user', 'name')

    if(!updateTodo) return res.status(422).json(errorMessage("failed to updated!"));
    res.status(200).json(success(updateTodo, "successfully update"));
}


// 5. Delete Todo Based on Id
function removeTodo(todo, elem){
    var index = todo.indexOf(elem);
    if(index > -1){
        todo.splice(index, 1);
    }
}

async function deleteTodo(req, res){
    let todo = await Todo.findById(req.params.id);
    console.log(todo)    
            /** DELETE TODO FROM USER SCHEMA */
    // get the user data from token
    user = await User.findOne({todo: req.params.id});
    // delete todo from user index
    removeTodo(user.todo, todo._id);

    // delete the todo from Todo Index
    const deleteTodo = await Todo.findByIdAndDelete(req.params.id)

    if(!deleteTodo) return res.status(422).json(errorMessage("Failed to Deleted Todo!"));
    res.status(200).json(success(deleteTodo, "successfully deleted!"))
}

module.exports = { createTodo, showTodo, show_by_id, updateTodo, deleteTodo}
