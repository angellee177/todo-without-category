const User = require('./../models/user');
const { success, errorMessage } = require('../helper/response');
// pick the variables we need
const _ = require('lodash');
// bcrypt the password
const bcrypt = require('bcryptjs');

// Register new User Function
async function Register(req, res){

    // to check if email already register
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(422).send(errorMessage('User already Registered.'));

    // create new user
    user = new User({name: req.body.name, email: req.body.email, password: req.body.password});
    
    // Encrypt the Password
    const saltRounds = 10;
    user.password = await bcrypt.hashSync(user.password, saltRounds)
    
    // save the User
    const result = await user.save();
    // response or output from function    
    res.status(200).json(success(result,"successfully register!"));
}


// Login Controller
async function Login(req, res){

    // check if email are not register
    let user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(421).json(errorMessage('Email are not Registered!'));

    // check if the password that store in DB same with the User input
    const validPassword = await bcrypt.compare(req.body.password, user.password)
    console.log(req.body.password, user.password)
    console.log(validPassword)
    if(!validPassword) return res.status(422).json(errorMessage("wrong password"))
    
    // generate json Token
    const token = user.generateAuthToken();
    res.status(200).json(success({token}," you have been success Login"))
};


// 3. Show All user
async function showAllUser(req, res){
    const userList = await User.find({}).populate({path: "todo", select: "task"});
    res.status(200).json(success(userList, "here is your user list: "))
}


// 4. Delete User by Id
async function deleteUser(req, res){
    const deleteUser = await User.findByIdAndDelete(req.params.id)
     
    res.status(200).json(success(`successfully delete ${deleteUser.name}`))
}


// 5. Show User by Token
async function current_user(req, res){

    // find the User Id based Token 
    let user = await User.findById(req.user._id).populate({path:'todo', select:'task'});
    // if token false
    if (!user) return res.status(404).json(errorMessage("Invalid user"));
    // the Token Found out
    res.status(200).json(success(_.pick(user, ['_id', 'name', 'email', 'todo']), "current user information:"))
}


// 6. Show User by Id
async function showbyId(req, res){
    let user = await User.findById(req.params.id).populate('todo', 'task');
    // if invalid user id
    if(!user) return res.status(422).json(errorMessage(`cannot find user with id ${req.params.id}`))
    // success find user by Id
    res.status(200).json(success(user, "user detail information:"))
}

module.exports = { Register, Login, showAllUser, deleteUser, current_user, showbyId };

