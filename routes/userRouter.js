const router = require('express').Router();
const userController = require('./../controllers/userController');
const auth = require('./../middleware/auth');

router.get('/', userController.showAllUser);
router.get('/todo/show', auth, userController.current_user);
router.get('/find/:id', auth, userController.showbyId);
router.post('/register', userController.Register);
router.post('/login', userController.Login);
router.delete('/delete/:id', auth, userController.deleteUser);

module.exports = router;

